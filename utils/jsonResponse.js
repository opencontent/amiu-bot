module.exports = function(res, detail, instance, statusCode) {
  statusCode = parseInt(statusCode, 10);
  let title = '';

  switch (statusCode) {
    case 200:
      title = 'OK';
      break;
    case 400:
      title = 'Bad Request';
      break;
    case 404:
      title = 'Not Found';
      break;
    case 500:
      title = 'Server Error';
      break;
    default:
      title = 'Unexpected Error';
  }

  // Enabling CORS
  res.header('Access-Control-Allow-Origin', '*');
  // Support header x-access-token for the authentication token
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, ' +
    'x-access-token, Authorization',
  );
  res.header('Content-Type', 'application/json');

  if (statusCode === 200) {
    res.status(statusCode).json(instance);
  } else {
    res.status(statusCode).send({
      title: title,
      detail: detail,
      status: statusCode,
      instance: instance,
    });
  }
};
