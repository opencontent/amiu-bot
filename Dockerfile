FROM node:12-alpine

RUN mkdir -p /srv

WORKDIR /srv

COPY package*.json /srv/

RUN npm install --only production

COPY . .

EXPOSE 8080 
EXPOSE 9000

ENV NODE_ENV=production

HEALTHCHECK --start-period=5s --interval=10s --timeout=5s CMD wget --spider http://localhost:9000/health || exit 1   

ENTRYPOINT [ "npm" ]
CMD [ "start" ]
