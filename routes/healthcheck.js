const express = require('express');
const bodyParser = require('body-parser');
const superagent = require('superagent');

const router = new express.Router();
const jsonResponse = require('../utils/jsonResponse');
const logger = require('../utils/logger')
require('dotenv').config();

router.get('/', async (_req, res, _next) => {
	// optional: add further things to check (e.g. connecting to dababase)
	const healthcheck = {
		uptime: process.uptime(),
		message: 'OK',
		timestamp: Date.now()
	};
	try {
		res.send(healthcheck);
	} catch (e) {
		healthcheck.message = e;
		res.status(503).send();
	}
});
// export router with all routes included
module.exports = router;

