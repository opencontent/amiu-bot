const express = require('express');
const bodyParser = require('body-parser');
const logger = require('./utils/logger');
const http = require('http');

const configuration = { detectKubernetes: false };
const lightship = require('lightship').createLightship(configuration);

require('dotenv').config();

const pjson = require('./package.json'); // Access to package.json


const app = express();

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  }),
);

app.get('/', (req, res) => {
  res.send('Welcome!');
});

// APIs
const webhook = require('./routes/webhook');
app.use('/webhook', webhook);

const server = app.listen(process.env.PORT || 8080, () => {
  // Lightship default state is "SERVER_IS_NOT_READY". Therefore, you must signal
  // that the server is now ready to accept connections.
  lightship.signalReady()

  logger.info(`Amiu Bot ${pjson.version}`);

  if (!(process.env.SENSOR_API_URL && process.env.SENSOR_USERNAME && process.env.SENSOR_PASSWORD)) {
    logger.error("Missing mandatory parameter: \"SENSOR_API_URL\" or \"SENSOR_USERNAME\" or \"SENSOR_PASSWORD\" is not defined")
    lightship.shutdown();
    //process.exit(1)
  }

  logger.info(
    'Amiu Bot is running on port ' +
    `${process.env.PORT || 8080}`);
});


// Lightship will start a HTTP service on port 9000.
lightship.registerShutdownHandler(() => {
  server.close();
});


