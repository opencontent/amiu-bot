const express = require('express');
const bodyParser = require('body-parser');
const superagent = require('superagent');

const router = new express.Router();
const jsonResponse = require('../utils/jsonResponse');
const logger = require('../utils/logger')
require('dotenv').config();

router.use(bodyParser.json());

router.route('/')
  // listConsents: list consents registered
  .post((req, res) => {
    let segnalazione = req.body;

    let mytype = process.env.POST_TYPE || 'informazioni';
    let mychannel = process.env.POST_CHANNEL || 'Telefono'; 

    if ('type' in segnalazione && segnalazione["type"] === mytype
      && 'author' in segnalazione && 'reporter' in segnalazione && segnalazione['author'] !== segnalazione['reporter']
      && 'channel' in segnalazione && segnalazione['channel'] === mychannel) {
      /*
      * If the type is mytype, the request was sent by an operator on behalf of the citizen and
      the channel is myhannel sets the reporter (the operator) as a reference for the citizen*/

      let url = `${process.env.SENSOR_API_URL}/posts/${segnalazione['id']}/approvers`;
      let payload = {
        "participant_ids": [segnalazione["reporter"]]
      }

      logger.info(`Updating post ${segnalazione['id']}} approvers with data ${payload}`)

      superagent
        .put(url)
        .auth(
          process.env.SENSOR_USERNAME,
          process.env.SENSOR_PASSWORD
        )
        .send(payload)
        .end((error, response) => {
          if (error) {
            logger.error({
              title: url,
              data: payload,
              status_message: 'An error occurred while updating post',
              error: error.response,
	      error_status: error.status,
            });
            jsonResponse(res, "An error occurred while updating post", payload, 500)
          } else {
            jsonResponse(res, "OK", "Post successfully updated", 200)
          }
        })

      let observerGroup = process.env.OBSERVER_GROUP_ID || 226;
      let putObserverUrl = `${process.env.SENSOR_API_URL}/posts/${segnalazione['id']}/observers`;
      let putObserverPayload = {
        "participant_ids": [observerGroup]
      }
      logger.info(`Updating post ${segnalazione['id']}} observers with data ${putObserverPayload}`)
      superagent
        .put(putObserverUrl)
        .auth(
          process.env.SENSOR_USERNAME,
          process.env.SENSOR_PASSWORD
        )
        .send(putObserverPayload)
        .end((error, response) => {
          if (error) {
            logger.error({
              title: putObserverUrl,
              data: putObserverPayload,
              status_message: 'An error occurred while updating post',
              error: error.response,
              error_status: error.status
            });
            jsonResponse(res, "An error occurred while updating post", putObserverPayload, 500)
          } else {
            jsonResponse(res, "OK", "Post successfully updated", 200)
          }
        })

    } else {
      jsonResponse(res, "OK", "Post skipped", 200)
    }
  });

module.exports = router;
