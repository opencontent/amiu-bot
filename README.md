# AMIU BOT

Amiu Bot uses OpenSegnalazioni webhooks to update newly created posts according to predetermined criteria:


The criteria are as follows:

* Type `richiesta informazioni`
* Channel `Telefono`
* Post opened by an operator on behalf of the citizen

When these three conditions occur simultaneously then the reporter will be imported as a reference for the citizen.

## Run
### Cli

    npm install
    npm start

### Docker

    docker-compose up -d --build

## Configuration


In order to interact with the OpenSegnalazioni API it is necessary to define some environmental variables

|   Field                            |   Description                                                         | Default                             |
| ---------------------------------- | --------------------------------------------------------------------- | ----------------------------------- |
| `SENSOR_API_URL`                   | OpenSegnalazioni API url                                              |   --                                |
| `SENSOR_USERNAME`                  | OpenSegnalazioni Username for API Authentication                      |   --                                |
| `SENSOR_PASSWORD`                  | OpenSegnalazioni Password for API Authentication                      |   --                                |
| `LOG_LEVEL`                        | Logs level                                                            |   info                              |
| `LOG_FORMAT`                       | Logs format (`json` or `text`)                                        |   json                              |
| `PORT`                             | Port on which to run the application                                  |   8080                              |
| `OBSERVER_GROUP_ID`                | Observer group id                                                     |   226                               |
| `POST_TYPE`    | Post type required to take action      |   informazioni      |
| `POST_CHANNEL` | Channel where post must be published in      |   Telefono      |

